## Initial steps

1. Run `php composer i`. Create .env.local and init vars:
```
DATABASE_URL
TWILIO_ACCOUNT_ID
TWILIO_ACCOUNT_TOKEN
TWILIO_PHONE_NUMBER
```
2. Create database `php bin/console doctrine:database:create`
3. Run migrations `php bin/console doctrine:migrations:migrate`
4. Run command `php bin/console app:fill:db` to fill sample data
5. Run
```
yarn install
yarn encore dev
```