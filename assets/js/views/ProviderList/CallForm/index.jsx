import React, {useEffect, useState} from "react";
import ApiService from "../../../api/ApiService";
import Spinner from "../../Spinner";

function CallForm() {
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [callSuccess, setCallSuccess] = useState(null);

  function callWithParams(event) {
    event.preventDefault();
    const accountSid = encodeURIComponent(event.target.elements.accountSid.value);
    const authToken = encodeURIComponent(event.target.elements.authToken.value);
    const twilioNumber = encodeURIComponent(event.target.elements.twilioNumber.value);
    const userNumber = encodeURIComponent(event.target.elements.userNumber.value);
    let status = 0;
    setIsLoading(true);

    fetch(`${ApiService.API_PREFIX}/call/${userNumber}?accountSid=${accountSid}&authToken=${authToken}&twilioNumber=${twilioNumber}`)
        .then(res => {
          status = res.status;
          return res.json();
        })
        .then(result => {
          setIsLoading(false);

          if (status < 400) {
            setCallSuccess(!!result.success)
          } else {
            setError(`Error status ${status}`);
          }
        })
        .catch(error => {
          setIsLoading(false);
          setError(error);
        })
    ;
  }

  useEffect(() => {
    setTimeout(() => {
      setError(null);
      setCallSuccess(null);
    }, 5000);
  }, [error, callSuccess])

  return (
      <>
        {isLoading && <Spinner/>}
        <form className="custom-form-call" onSubmit={callWithParams}>
          <h4>Custom call form</h4>
          <div className="row">
            <div className="form-group col-sm-6 col-md-3">
              <input type="text" className="form-control" id="accountSid" placeholder="Account SID" required={true}/>
            </div>
            <div className="form-group col-sm-6 col-md-3">
              <input type="text" className="form-control" id="authToken" placeholder="Auth token" required={true}/>
            </div>
            <div className="form-group col-sm-4 col-md-2">
              <input type="text" className="form-control" id="twilioNumber" placeholder="Twilio number"
                     required={true} pattern="^\+\d+$"/>
            </div>
            <div className="form-group col-sm-4 col-md-2">
              <input type="text" className="form-control" id="userNumber" placeholder="User number"
                     required={true} pattern="^\+\d+$"/>
            </div>

            <div className="form-group col-sm-4 col-md-2">
              <button className="btn btn-primary w-100" type="submit">Call</button>
            </div>
          </div>
          {callSuccess === true && <div className="alert alert-success">Success</div>}
          {error !== null && <div className="alert alert-danger">{error}</div>}
        </form>
      </>
  );
}

export default CallForm;
