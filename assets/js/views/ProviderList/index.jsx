import React, {useState, useEffect} from "react";
import ReactDOM from "react-dom";
import ApiService from '../../api/ApiService';
import CallForm from "./CallForm";
import Spinner from "../Spinner";

function ProviderList() {
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [items, setItems] = useState([]);
  const [callSuccess, setCallSuccess] = useState(null);

  useEffect(() => {
    setIsLoading(true);
    fetch(`${ApiService.API_PREFIX}/provider-list`)
        .then(res => res.json())
        .then(result => {
          setIsLoading(false);
          setItems(result);
        })
        .catch(error => {
          setIsLoading(false);
        })
  }, [])

  useEffect(() => {
    setTimeout(() => {
      setError(null);
      setCallSuccess(null);
    }, 5000);
  }, [error, callSuccess])

  function makeCall(number) {
    setIsLoading(true);
    let status = 0;

    fetch(`${ApiService.API_PREFIX}/call/${number}`)
        .then(res => {
          status = res.status;
          return res.json();
        })
        .then(result => {
          setIsLoading(false);

          if (status < 400) {
            setCallSuccess(!!result.success)
          } else {
            setError(`Error status ${status}`);
          }
        })
        .catch(error => {
          setIsLoading(false);
          setError(error);
        })
    ;
  }

  return (
      <>
        {isLoading && <Spinner/>}
        <div className={"alert alert-danger top-alert" + (error !== null ? ' top-alert__visible' : '')} role="alert">{error}</div>
        <div className={'alert alert-success top-alert' + (callSuccess === true ? ' top-alert__visible' : '')} role="alert">Success</div>
        <div className="panel-rounded">
          <h3>Matched providers</h3>
          <table className="table table-striped table-hover">
            <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th/>
            </tr>
            </thead>
            <tbody>
            {items.map((value, index) => {
              return (
                  <tr key={index}>
                    <td>{value.id}</td>
                    <td>{value.name}</td>
                    <td>{value.email}</td>
                    <td>{value.phone}</td>
                    <td>
                      <button
                          onClick={() => makeCall(value.phone)}
                          className="btn btn-success"
                      >Call
                      </button>
                    </td>
                  </tr>
              )
            })}
            </tbody>
          </table>

          <CallForm/>
        </div>
      </>
  );
}

const $el = document.getElementById("provider-list");
ReactDOM.render(
    <ProviderList/>,
    $el
);
