<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Provider;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FillDatabaseCommand extends Command
{
    protected static $defaultName = 'app:fill:db';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(
        EntityManagerInterface $em
    )
    {
        parent::__construct(null);
        $this->em = $em;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $data = [
            [
                'name' => 'Lucile Munoz',
                'email' => 'oceane.torp@brennan.us',
                'phone' => '+17278091539',
            ],
            [
                'name' => 'Winifred Henderson',
                'email' => 'landen_deckow@hotmail.com',
                'phone' => '+15328315321',
            ],
            [
                'name' => 'Birdie Carson',
                'email' => 'dietrich_amiya@yahoo.com',
                'phone' => '+16732237397',
            ],
            [
                'name' => 'Family Care SF, Inc.',
                'email' => 'dorothy.lesch@dickinson.me',
                'phone' => '+18661245438',
            ],
            [
                'name' => 'Barry Alexander',
                'email' => 'reilly.julius@gmail.com',
                'phone' => '+18851639766',
            ],
        ];
        foreach ($data as $row) {
            $provider = new Provider();
            $provider
                ->setName($row['name'])
                ->setEmail($row['email'])
                ->setPhone($row['phone'])
            ;
            $this->em->persist($provider);
        }

        $this->em->flush();
        $output->writeln('Database filled with sample data');

        return 0;
    }
}
