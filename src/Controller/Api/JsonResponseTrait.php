<?php

declare(strict_types=1);

namespace App\Controller\Api;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

trait JsonResponseTrait
{
    public function simpleResponse(array $data, $responseCode = Response::HTTP_OK): JsonResponse
    {
        return new JsonResponse($data, $responseCode);
    }

    public function errorResponse(array $errors, $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR): JsonResponse
    {
        return new JsonResponse(['errors' => $errors], $statusCode);
    }
}
