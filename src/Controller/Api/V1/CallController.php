<?php

declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Controller\Api\JsonResponseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Twilio\Rest\Client;

class CallController extends AbstractController
{
    use JsonResponseTrait;

    /**
     * @Route("/call/{phoneNumber}", name="api_v1_call", requirements={"phoneNumber": "^\+\d+$"})
     */
    public function call(Request $request, string $phoneNumber): JsonResponse
    {
        $sid = $request->get('accountSid', $_SERVER['TWILIO_ACCOUNT_ID'] ?? null);
        $token = $request->get('authToken', $_SERVER['TWILIO_ACCOUNT_TOKEN'] ?? null);
        $fromNumber = $request->get('twilioNumber', $_SERVER['TWILIO_PHONE_NUMBER'] ?? null);
        $client = new Client($sid, $token);

        try {
//            $client->calls->create(
//                $phoneNumber,
//                $fromNumber,
//                ['url' => 'http://demo.twilio.com/docs/voice.xml']
//            );

        } catch (\Throwable $e) {
            return $this->errorResponse([\get_class($e) => $e->getMessage()]);
        }

        return $this->simpleResponse(['success' => true]);
    }
}
