<?php

declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Controller\Api\JsonResponseTrait;
use App\Entity\Provider;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ProviderListController extends AbstractController
{
    use JsonResponseTrait;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var NormalizerInterface|SerializerInterface
     */
    private $serializer;

    public function __construct(
        EntityManagerInterface $entityManager,
        SerializerInterface $serializer
    )
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/provider-list", name="api_v1_provider_list")
     */
    public function providerList(): JsonResponse
    {
        /** @var Provider[] $providers */
        $providers = $this->entityManager->getRepository(Provider::class)->findAll();

        $data = [];
        foreach ($providers as $provider) {
            $data[] = $this->serializer->normalize($provider);
        }

        return $this->simpleResponse($data);
    }
}
